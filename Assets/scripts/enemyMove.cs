﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMove : MonoBehaviour
{

	public float speed;
	public GameObject Player;


	private void Start()
	{
		Player = GameObject.FindWithTag("Player");
	}

	void Update ()
	{
		move();
	}

	void move()
	{
		transform.LookAt(Player.transform);
		if (Vector3.Distance(transform.position, Player.transform.position) > 2f)
		{
			transform.Translate(Vector3.forward * speed * Time.deltaTime);
		}
	}
}
