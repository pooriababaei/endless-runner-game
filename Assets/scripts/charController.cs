﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class charController : MonoBehaviour
{
	public bool runner;
	private Vector2 input;
	public float rotationSpeed;
	public float speed;
	public Animator anim;
	public bool isGrounded;
	public float jumpSpeed;
	public Rigidbody body;

	private void Start()
	{
		anim = GetComponent<Animator>();
		body = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		
		inputHandler();
		rotation();
		move();
		jump();
		animtionController();
	}

	
	void inputHandler()
	{
		input.Set(Input.GetAxis("Horizontal" )  , Input.GetAxis("Vertical"));
	}	
	
	void rotation()
	{
		Vector3 targetDirection = new Vector3(input.x , 0 , input.y);
		if (input != Vector2.zero && targetDirection.magnitude > 0)
		{
			Vector3 lookdirection = targetDirection.normalized;
			Quaternion rotateDir = Quaternion.LookRotation(lookdirection, Vector3.up);
			transform.rotation = Quaternion.Lerp(transform.rotation , rotateDir , rotationSpeed * Time.deltaTime);
		}
	}

	void move()
	{
		float forward ;
		forward = input.magnitude;
		if (forward > 1)
		{
			forward = 1;
		}
		Vector3 movement = new Vector3(0 , 0 , forward);
		transform.Translate(movement * speed * Time.deltaTime);
	}

	void jump()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			anim.SetBool("jump" , true);
			//jump;
			Vector3 vel = body.velocity;
			vel.y = jumpSpeed;
			body.velocity = vel;
			
		}
	}

	void animtionController()
	{
		if (input.magnitude > 0)
		{
			anim.SetBool("run" , true);
		}
		else
		{
			anim.SetBool("run" , false);
		}
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.transform.tag == "ground")
		{
			anim.SetBool("jump" , false);
			isGrounded = true;
		}
	}

	private void OnCollisionExit(Collision other)
	{
		if (other.transform.tag == "ground")
		{
			isGrounded = false;
		}
	}
}
