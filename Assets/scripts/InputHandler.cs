﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {

	 public string horizontalInput = "Horizontal";
     public string verticallInput = "Vertical";
     public KeyCode jumpInput = KeyCode.Space;
     public KeyCode sprintInput = KeyCode.LeftShift;
     public charMove cm;

     private void Awake()
     {
	     cm = GetComponent<charMove>();
     }
     
     protected virtual void LateUpdate()
     {
	     if (cm == null) return;             // returns if didn't find the controller		    
	     InputHandle();                      // update input methods
	                   
     }

     void InputHandle()
     {
	     moveCharacter();
	     SprintInput();
	     JumpInput();
     }

     public void moveCharacter()
     {
	     cm.input.x = Input.GetAxis(horizontalInput);
	     cm.input.y = Input.GetAxis(verticallInput);
     }

     public void SprintInput()
     {
	     if (Input.GetKeyDown(sprintInput))
	     {
		     cm.isSprinting = true;
	     }
	     else if (Input.GetKeyUp(sprintInput))
	     {
		     cm.isSprinting = false;
	     }
     }

     public void JumpInput()
     {
	     if (Input.GetKeyDown(jumpInput))
		     cm.jump();
     }
}
