﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyGenerato : MonoBehaviour
{

	public GameObject enemy;
	public GameObject[] spawnPoints;
	private int randomIndex;

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.E))
		{
			createEnemy();
		}
	}

	private void Start()
	{
		InvokeRepeating("createEnemy" , 3 , 2);
	}

	void createEnemy()
	{
		randomIndex = Random.Range(0, 3);
		GameObject o = Instantiate(enemy, spawnPoints[randomIndex].transform.position, Quaternion.identity);
	}
}
