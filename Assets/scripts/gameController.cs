﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameController : MonoBehaviour
{

	public GameObject tile;
	public GameObject cloud;
	public Dictionary<int , GameObject> tiles;
	public GameObject lastTile;
	public GameObject lastCloud;
	public static gameController refrence;
	public GameObject[] obs;
	public GameObject[] spawnPoint;
	private bool canCreate;
	private float counter = 0;
	private int rr = 0;
	public float speed = 1;
	private float speedCounter = 0;
	public float score = 0;
	public Text scoreT;
	public GameObject loosePanel;
	public Text lastScore;
	private int sscore;
	private void Awake()
	{
		Time.timeScale = 1;
		if (refrence == null)
		{
			refrence = this;
		}
		
		tiles = new Dictionary<int, GameObject>();
	}

	private void Update()
	{
		if (lastTile.transform.position.z < 20)
		{
			gameController.refrence.createTile();
		}

		if (lastCloud.transform.position.z < 50)
		{
			
			createCloude();
		}

		counter += Time.deltaTime;
		if (counter >= 5/speed)
		{
			counter = 0;
			createObs();
		}

		speedCounter += Time.deltaTime;
		if (speedCounter > 10)
		{
			speedCounter = 0;
			if (speed < 5)
			{
				speed += .3f;
			}
		}

		score += Time.deltaTime;
		sscore = (int) score;
		scoreT.text = "Score : "+ sscore.ToString();
	}
	


	public void createTile()
	{
		GameObject o = Instantiate(tile, lastTile.transform.position, Quaternion.identity);
		lastTile = o;
		Vector3 pos = o.transform.position;
		pos.z = lastTile.transform.position.z + 40;
		o.transform.position = new Vector3(pos.x , pos.y , pos.z);
		/*if (canCreate)
		{
			createObs();
		}*/

		canCreate = true;
	}

	public void createCloude()
	{
		GameObject o = Instantiate(cloud, lastCloud.transform.position, Quaternion.identity);
		lastCloud = o;
		Vector3 pos = o.transform.position;
		pos.z = lastCloud.transform.position.z + 300;
		o.transform.position = new Vector3(pos.x , pos.y , pos.z);
	}


	public void createObs()
	{
		if (canCreate)
		{
			int r = Random.Range(0, obs.Length - 1);
			
			//rr = Random.Range(0, spawnPoint.Length);
			
			GameObject o = Instantiate(obs[r], spawnPoint[rr].transform.position, Quaternion.identity);
			if (rr < spawnPoint.Length-1)
			{
				rr++;
			}
			else
			{
				rr = 0;
			}
		}
	}

	public void loose()
	{
		lastScore.text ="Score : "+ sscore.ToString();
		Time.timeScale = 0;
		loosePanel.SetActive(true);
	}

	public void Reset()
	{
		SceneManager.LoadScene(0);
	}
}
