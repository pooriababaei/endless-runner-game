﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class charMove : MonoBehaviour
{
	private Vector3 movement;
	private Vector3 targetDirection;
	public float speed;
	public float sprintSpeed;
	public float freeRotationSpeed;
	public float jumpSpeed;
	public bool isJumping;
	public bool isSprinting;
	public Vector2 input;
	public Animator anim;
	public Rigidbody body;

	public static charMove refrence;

	private void Awake()
	{
		if (refrence == null)
		{
			refrence = this;
		}
	}

	// Use this for initialization
	void Start ()
	{
		
		anim = GetComponent<Animator>();
		body = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update ()
	{
		//move();
		animatorController();
		rotation();
		
	}

	void move()
	{
		//movement.Set(input.x , 0 , Math.Abs(input.y));
		//float forward = Mathf.Clamp(input.magnitude, 0, 1);
		movement.Set(input.x,0, 0);
		transform.Translate(movement * (isSprinting? sprintSpeed : speed) * Time.deltaTime , Space.World);
	}

	public void jump()
	{
		if (!isJumping)
		{
			anim.SetBool("jump" , true);
			isJumping = true;
			print("jump");
			var velocity = body.velocity;
			velocity.y = jumpSpeed;
			body.velocity = velocity;
		}
		
	}


	void rotation()
	{
		
		targetDirection = new Vector3(input.x, 0, 0);
		if (input != Vector2.zero && targetDirection.magnitude > 0.1f)
		{
			
			Vector3 lookDirection = targetDirection.normalized;
			Quaternion freeRotation = Quaternion.LookRotation(lookDirection, transform.up);		
			transform.rotation = Quaternion.Lerp(transform.rotation, freeRotation, freeRotationSpeed * Time.deltaTime);
		}
	}

	public void animatorController()
	{
		if (input.magnitude > 0)
		{
			anim.SetBool("run" , true);
		}
		else
		{
			anim.SetBool("run" , false);
		}

		if (isSprinting)
		{
			anim.SetFloat("speed" , 1.3f);
		}
		else
		{
			anim.SetFloat("speed" , 1.0f);
		}
	}


	private void OnCollisionEnter(Collision other)
	{
		if (other.transform.tag == "ground")
		{
			isJumping = false;
			anim.SetBool("jump" , false);
		}
	}
}
