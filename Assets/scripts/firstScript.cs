﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class firstScript : MonoBehaviour
{

	public int speed;
	private int speed2;

	private void Start()
	{
		speed = 2;
	}

	private void Update()
	{
		moveController();
	}

	void moveController()
	{
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			//print("left");
			//transform.position = transform.position + new Vector3(speed * Time.deltaTime, 0, 0);
			transform.position = new Vector3(transform.position.x - speed*Time.deltaTime , transform.position.y , transform.position.z);
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			//print("right");
			transform.position = new Vector3(transform.position.x + speed*Time.deltaTime , transform.position.y , transform.position.z);
		}
		else if (Input.GetKey(KeyCode.UpArrow))
		{
			//print("Up");
			transform.position = new Vector3(transform.position.x  , transform.position.y + speed*Time.deltaTime, transform.position.z);
		}
		else if (Input.GetKey(KeyCode.DownArrow))
		{
			//print("Down");
			transform.position = new Vector3(transform.position.x  , transform.position.y - speed*Time.deltaTime, transform.position.z);
		}
	}
}
