﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class charRunner : MonoBehaviour
{

	public float speed;
	public float jumpSpeed;
	public Animator anim;
	public Rigidbody body;
	public GameObject rightLine;
	public GameObject leftLine;
	public bool isMoving;
	public bool isGrounded;
	public GameObject ground;

	private void Awake()
	{
		anim = GetComponent<Animator>();
		body = GetComponent<Rigidbody>();
		
	}

	private void Start()
	{
		anim.SetBool("run" , true);
	}


	private void Update()
	{
		moveController();
		jump();

		if (ground != null)
		{
			print(Math.Abs(transform.position.y - ground.transform.position.y));
			if (Math.Abs(transform.position.y - ground.transform.position.y) < .6f)
			{
				
				if(!isGrounded)
				anim.SetBool("jump", false);
				isGrounded = true;
			}
			else
			{
				isGrounded = false;
			}
		}
	}

	void moveController()
	{
		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			if (transform.position.x < rightLine.transform.position.x - 1f)
			{
				if (!isMoving)
				{
					isMoving = true;
					Vector3 target = new Vector3(transform.position.x+5 , transform.position.y , transform.position.z);
					StartCoroutine(move(target));							
				}
			}
		}
		else if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			if (transform.position.x > leftLine.transform.position.x + 1f)
			{
				if (!isMoving)
				{
					isMoving = true;
					Vector3 target = new Vector3(transform.position.x-5 , transform.position.y , transform.position.z);
					StartCoroutine(move(target));					
				}
			}
		}
	}

	IEnumerator move(Vector3 targetPos)
	{
		while (Math.Abs(targetPos.x - transform.position.x)>0.5f)
		{
			float xx = Mathf.Lerp(transform.position.x , targetPos.x , Time.deltaTime * speed * gameController.refrence.speed);
			transform.position = new Vector3(xx , transform.position.y , transform.position.z);
			yield return null;		
		}
		//transform.position = new Vector3(targetPos.x , transform.position.y , transform.position.z);
		isMoving = false;
		yield return null;
	}


	void jump()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (isGrounded)
			{
				
				anim.SetBool("jump", true);
				Vector3 vel = body.velocity;
				vel.y = jumpSpeed;
				body.velocity = vel;
			}
		}
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.transform.tag == "ground")
		{
			//isGrounded = true;
			//anim.SetBool("jump" , false);
		}

		/*if (other.transform.tag == "obs")
		{
			print("failed");
		}*/
	}

	private void OnCollisionExit(Collision other)
	{
		if (other.transform.tag == "ground")
		{
			//isGrounded = false;
			
		}
	}
	
	
	private void OnCollisionStay(Collision other)
	{
		if (other.transform.tag == "ground")
		{
			ground = other.gameObject;
			//isGrounded = true;
			//anim.SetBool("jump", false);
		}
	}


	private void OnTriggerEnter(Collider other)
	{
		if (other.transform.tag == "obs")
		{
			print("loose");
			gameController.refrence.loose();
		}
	}
}
