# Endless 3D Runner Game in Unity

In this project, we designed and developed a runner game, like Subway Surfers in Unity. It never ends unless the player collides to obstacles. We generated the ground and the obstacles automatically and randomly. To make it endless and prevnet from ram overflow, we implemented Object Pool design pattern.

![GamePlay](runner-gameplay.png)

## Authors

- [**Pooria Babaei**](https://gitlab.com/pooriababaei)
- [**Mohammadreza Tajerian**](https://gitlab.com/momad200)
